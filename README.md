# HSE-Application-Template

Labview的一个通用程序开发模板，核心是DQMH。
本仓库用于对这个框架模板进行学习。
项目支持 LV 16-19。20主要是需要解决插件依赖问题。

## :rocket: 安装

See our detailed guide on https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template/setup

根据上面的说明操作就可以。

主要就是 装好labview，默认安装就可以，如果定制安装去掉一些组件的话，可能会有问题，因为比如数据库部分不装会引起这个框架的数据库部分提示缺库。

然后 运行 非源码资源文件夹下的 hse-application-template.vipe 然后根据提示安装必要的插件包。 主要是 dqmh的包。hse的有2个。

完成之后，就可以打开 TEMPLATE.lvproj  文件了。

检验环境是否安装好的方法也很简单，就是看是否能正确打开上面的工程文件。

## 定制自己的工程

首先 根据 [安装说明](https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template/setup)

修改项目为Study

修改相应的文件夹名，修改 constant name 那个vi里面的 变量值

修改 发布的一些信息，主要是 名称 ，以及 语言里面 添加中文 去掉德文。

:important: 打开新的 study 项目后，看到的是，里面包含的文件夹并没有变化，还是过去的template的，这时候可以右键 转换到自动更新的文件夹 方式重新定义文件夹位置。 更新位置后，取消更新就可以了。

两个文件夹都如此操作，这里操作后，会出现一些报错，我个人感觉那些错误是正常出现的，可以删除，因为找不到的是过去的那些文件，那些文件目前的工程里面我们已经不需要了。

之后就可以正常打开项目了。

然后 如果 在 config 目录下 ，也同样的修改了 私有配置文件夹的名字，比如修改为Unit_StudyUnitId的话，那还要再次的 自动更新一下文件夹，然后再停止，感觉这个自动更新的功能对于处理项目内的资源变动是非常有用的。

## 后期初步的学习思路

整体而言，这是一个基于DQMH的应用开发模板，里面的Framework 和 Libraries 主要都是通过DQMH方式实现的。然后 Modules里面是几个例子，所以从感觉上，应该是 Modules这例子里面的模块 如何 与整个框架进行通信和协作的。

所以 对于 Modules下面的每一个 模块 ，可以打算建立一个分支，然后对其进行配置和学习，也就是把问题简单化，然后逐个学习，最后经历过多个DQMH模块的学习后，回到master分支，做整体的修改和学习。

### Dummie 模块的学习

这个模块主要用途是做 配置 设置用，动态界面调控，另外clone也是在这里调用的。

### 关于HSE DQMH 与 原生 DQMH的差异

[HSE DQMH® Flavour](https://dokuwiki.hampel-soft.com/kb/labview-frameworks/dqmh/hse-flavour)

# HAMPEL SOFTWARE ENGINEERING's Windows Application Template

A demo Windows application based on our UI framework, which builds on the [HSE DQMH flavor](https://dokuwiki.hampel-soft.com/code/dqmh/hse-flavour) of the Delacor Queued Message Handler (DQMH, www.delacor.com) and facilitates our [HSE Libraries](https://dokuwiki.hampel-soft.com/code/hse-libraries). With basic functionality for module loading and configuration, UI management and logging/debugging.

Features include:
* Dynamic loading of DQMH modules (no static references to UI Framework code)
* User Interface module for managing runtime menus, subpanels etc.
* Navigation module for displaying dynamically created harmonica menus
* Event Manager module for showing real-time debug information 

UI Framework and HSE Libraries bring even more goodness, eg:
* Dynamic module-to-module communication through generic broadcasts ("System Messages")
* Low-level logging through the HSE Logger 
* and many more!

## :bulb: Documentation

The HSE Dokuwiki holds more information:

* https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template
* https://dokuwiki.hampel-soft.com/code/dqmh/hse-flavour
* https://dokuwiki.hampel-soft.com/code/hse-libraries
* https://dokuwiki.hampel-soft.com/code/open-source/hse-db

### :question: FAQ
See our [FAQ](https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template/faq) for comments on updating versions amongst other things.



### :link: Dependencies

Apply the VI Package Configuration File from `/hse-application-template.vipc`

This demo application depends on :
* https://dokuwiki.hampel-soft.com/code/open-source/hse-libraries
* https://dokuwiki.hampel-soft.com/code/open-source/hse-db

### :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.


## :busts_in_silhouette: Contributing 

The Modules and Module Templates in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com).

Please get in touch with us at (office@hampel-soft.com) or visit our website (www.hampel-soft.com) if you want to contribute.

##  :beers: Credits

* Joerg Hampel
* Manuel Sebald
* Bence Bartho
* Danny Thomson
* John Gebraeel
* Benjamin Hinrichs
* Julian Schary
* Alexander Elbert

## :page_facing_up: License 

This repository and its contents are licensed under a BSD/MIT like license - see the [LICENSE](LICENSE) file for details
