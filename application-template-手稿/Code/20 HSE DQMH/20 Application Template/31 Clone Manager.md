#31 克隆管理器  
这不是模板的一部分，而是需要时遵循的概念。  
如果您希望应用程序本身（即 startup.vi）或 UI 管理器加载多个克隆，建议或首选方法是创建一个克隆管理器 DQMH 模块。由于这 - 根据我们的经验 - 或多或少具有特定于项目的要求，我们没有考虑创建通用克隆管理器，而是构建特定于应用程序的模块。  
只需进行一些更改，任何可克隆模块都支持以下建议的架构：  
克隆管理器:  
克隆管理器模块可以……  
*有自己的 .ini 文件，像其他模块一样，告诉它要启动哪个和多少个克隆.  
每个克隆都由一个克隆名称标识.  
克隆名称在管理器的 .ini 文件中定义.  
克隆名称在启动时移交给每个克隆.  
*管理克隆注册表  
*包含多个子面板以并行显示多个克隆  
在注册表中管理克隆到子面板的分配.  
![](https://dokuwiki.hampel-soft.com/_media/code/dqmh/hse-application-template/clone-manager-1.png?w=600&tok=0c582d)  
克隆:  
克隆模块…  
需要在其前面板上进行控制，以便可以在启动期间提供克隆名称.  
有一个共享的 .ini 文件.  
每个克隆实例一个部分（由克隆名称标识）.  
从 .ini 文件中的相应部分读取自己的配置.  
![](https://dokuwiki.hampel-soft.com/_media/code/dqmh/hse-application-template/clone-manager-2.png?cache=&w=900&h=359&tok=3bbdaf)