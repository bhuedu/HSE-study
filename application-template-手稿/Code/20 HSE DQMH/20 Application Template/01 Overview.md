# application template

## 01 overview 综述

此应用程序模板展示了我们用于具有图形用户界面的应用程序的 UI 框架。 我们将此模板用作容器，从配置文件动态加载项目特定的 DQMH 模块。

![](./img/20210305-110517.png)

**UI 框架**

UI 框架帮助我们：

1. 使用 UI 管理代码

2. 特定于项目的 UI 布局

3. 显示启动画面

4. 动态填充运行时菜单

5. 动态填充导航模块

6. 生成用于调试的事件日志

等等

  UI 框架建立在 HSE 库之上，由一组 DQMH 模块和一些辅助 VI 组成。

**框架模块**

这些 DQMH 模块旨在重复使用，因此它们是通用的，而不是应用程序特定于项目的代码的一部分：

1. 事件管理器

2. UI 管理

3. 导航

**框架 VI**

为了动态加载 DQMH 模块并实现一些 UI 框架功能，以下 VI 也是框架的一部分：

*启动VI*

/startup.vi 用于运行应用程序。

1. 显示启动画面

2. 读取主要配置（包含要加载的模块列表）

3. 加载 UI 管理器和事件管理器

4. 模块加载项目模块列表（无静态链接）

5. 调用所有模块的“配置”

6. 请求显示在前面 UI 管理器模块的面板

**项目VIS**

这些是 UI 框架提供的与项目相关或特定于项目的 VI，但可以或必须针对每个项目进行修改。

  /Project/Project.lvlib：包含项目特定的 VI
   /Project/PROJECT_InitLogging.vi：启动 hse-logger 
   /Project/PROJECT_Name–constant.vi：项目名称 
   /Project/PROJECT_RunTimeMenu.rtm：运行时菜单 应用程序 
   /Project/PROJECT_SplashScreen.vi：启动画面
   /Project/PROJECT_StartupSteps.ctl：startup.vi 执行的步骤列表
  /Project/PROJECT_UserCredentials.vi：内置登录功能的用户列表

   **特定于项目的 DQMH 模块**

   为了使 DQMH 模块与我们的应用程序模板兼容，这些模块需要实现我们的 HSE DQMH 风格。