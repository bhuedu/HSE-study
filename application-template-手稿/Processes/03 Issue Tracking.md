# 03 Issue Tracking  03 问题追踪  
> 问题跟踪系统（也称为故障单或请求管理系统）根据组织的需要管理和维护问题列表。 客户支持通常使用问题跟踪系统来创建、更新和解决报告的客户问题，甚至是该组织的其他员工报告的问题。  
 
## Introduction  
由于我们的代码存储库是在 GitLab 服务器上管理的，因此我们使用了 GitLabs 集成的问题跟踪器。 拥有一个统一的源代码版本控制和问题跟踪平台，让我们能够以一种非常简单自然的方式将问题与代码修订联系起来。  
## Issue Tracking with GitLab  使用 GitLab 跟踪问题  
如果您有一个有效的 GitLab 用户并且我们授予您访问 GitLab 存储库的权限，您可以直接从 GitLab Web 界面管理问题  
## Labels  标签  
您可以在 https://gitlab.com/groups/hampel-soft/-/labels 找到标签列表及其含义。  