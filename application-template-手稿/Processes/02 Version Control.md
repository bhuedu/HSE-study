# 02 Version Control  
   
> 版本控制，也称为修订控制和源代码控制，是对信息更改的管理。 版本控制系统在整个开发过程中跟踪软件版本。 分布式版本控制系统允许许多开发人员处理给定的项目，而无需他们保持与公共网络的连接。   
 
#  Introduction 介绍  
我们使用 Git 进行分布式 LabVIEW 代码版本控制。 我们的代码存储库在 GitLab 服务器上进行外部管理，在 Git 服务器上进行内部管理。  

<strong>为了能够处理我们的 LabVIEW 代码，您需要按照我们的 Git 知识库的设置部分中描述的步骤进行操作：<strong> 


[01 Setup](../Knowledge%20Base/01%20Best%20Practices/Way%20Of%20Working/10%20Setup.md)   
[02 Basic Usage](../Knowledge%20Base/12%20Source%20Code%20Control/Git/02%20Basic%20Usage.md)   
[03 Further Resources](../Knowledge%20Base/12%20Source%20Code%20Control/Git/03%20Further%20Resources.md)    
[04 Debugging](../Knowledge%20Base/21%20NI%20LabVIEW/99%20Debugging.md)   
[05 Subversion Migration](../Knowledge%20Base/12%20Source%20Code%20Control/Git/05%20Subversion%20Migration.md)    
[Git: Bundle](../Knowledge%20Base/12%20Source%20Code%20Control/Git/Git%20%20Bundle.md)  
[Git: LFS](../Knowledge%20Base/12%20Source%20Code%20Control/Git/Git%20LFS.md)   
[Git: Single Commit Clone](../Knowledge%20Base/12%20Source%20Code%20Control/Git/Git%20Single%20Commit%20Clone.md)    
[Git: Split Repositories](../Knowledge%20Base/12%20Source%20Code%20Control/Git/Git%20Split%20Repositories.md)    
[Git: Submodules](../Knowledge%20Base/12%20Source%20Code%20Control/Git/Git%20Submodules.md)  
[Git: Tags](../Knowledge%20Base/12%20Source%20Code%20Control/Git/Git%20Tags.md)   

01 设置 

02 基本用法

 03 更多资源 
 
 04 调试 
 
 05 Subversion 迁移
 
Git：捆绑 
  
Git：LFS
  
Git：单提交克隆
   
Git：拆分存储库 

Git：子模块
    
Git：标签

## SCC Best Practices  SCC 最佳实践  
您可以在我们的最佳实践部分找到有关如何使用（在）源代码控制中的更多信息：  
[01 Rules & Guides](../Knowledge%20Base/01%20Best%20Practices/Source%20Code%20Control/01%20Rules%20&%20Guides.md)   
[10 Workflow](../Knowledge%20Base/01%20Best%20Practices/Source%20Code%20Control/10%20Workflow.md)  
[11 Gitflow Workflow](../Knowledge%20Base/01%20Best%20Practices/Source%20Code%20Control/11%20Gitflow%20Workflow.md)   
[12 Project Forking Workflow](../Knowledge%20Base/01%20Best%20Practices/Source%20Code%20Control/12%20Project%20Forking%20Workflow.md)  
[13 Fork Setup](../Knowledge%20Base/01%20Best%20Practices/Source%20Code%20Control/13%20Fork%20Setup.md)  
01 规则与指南    
10 工作流程   
11 Gitflow 工作流程   
12 项目分叉工作流程   
13 分叉设置   
## Version Numbering  版本编号  
请阅读版本号以获取有关我们的版本号格式和政策的更多信息。