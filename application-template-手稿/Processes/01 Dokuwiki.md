# 01 Dokuwiki    多库维基  
> 维基允许协作修改、扩展或删除内容和结构。 通常，它是一个 Web 应用程序，文本是使用简化的标记语言编写的。  DokuWiki 是一个简单易用且用途广泛的开源维基软件。  

Dokuwiki 基于网络的特性使我们能够以一种非常易于访问的方式提供信息。 它还支持与项目特定信息的轻松协作，为您提供创建和使用信息的平等机会。  
## Structure 结构  
我们的 Dokuwiki 分为四个部分：  
### Processes 进程  
涵盖有关如何与我们合作的信息。  
### Code  代码  
包含有关我们公共/开源产品的信息（另见 code.hampel-soft.com）。  
### Knowledge Base  知识库  
包含与我们的工作相关的各种主题的各种信息。  
### Projects 项目  
保存特定于项目的信息，如协议、描述、手册……只能使用客户特定的用户凭据访问。  
## How to edit content  如何编辑内容  
编辑现有内容：如果您有足够的权限来编辑页面，您会在页面右侧找到一个小钢笔图标。 如果您用鼠标箭头将鼠标悬停在它上面，它会变成“编辑此页面”链接：  

![](img/how-to-edit.png)

创建新页面：只需在浏览器的地址栏中键入您希望新内容具有的 URL 并加载该新页面。 在新页面上，“编辑此页面”链接将变为“创建此页面”链接。

![](img/how-to-new-url.png)  

![](img/how-to-create.png)  

注意：要编辑现有内容或创建新内容，您必须拥有足够的用户权限。  

* 编辑模式   

在编辑模式下，会显示一个大表单域。 您可以在其中输入您的内容并通过单击其下方的“保存”按钮提交更改。  

![](img/how-to-editor.png)

* 语法    
  
 DokuWiki 支持一些简单的标记语言，它试图使数据文件尽可能具有可读性。 此页面包含您在编辑页面时可能使用的所有可能的语法。  
*  PlantUML  
  
  我们的 Dokuwiki 还支持从 Markdown 语法生成 PlantUML 图。 一个例子：

   <uml>
  @startuml  

  hide empty description

  [*] --> State1

  State1 --> [*]

  State1 : this is a string

  State1 : this is another string
  
  State1 -> State2
  State2 --> [*]
  @enduml
  </uml>  
  表示为  
  ![](img/1.png)

  另一个有用的文件夹和文件演示：  
  ![](img/2.png)  

有关 UML 语法的更多信息，请访问 https://plantuml.com/   
## User Manual  使用手册   

用户手册旨在成为有关使用 DokuWiki 的所有标准问题的扩展资源。 它包括对“标准”用户以及想要配置和运行他们自己的 wiki 安装的管理员有用的信息。

# DokuWiki API  
实现了一个 XML RPC API，一个 JSON RPC API 可用作插件。  
https://www.dokuwiki.org/devel:xmlrpc
https://www.dokuwiki.org/plugin:jsonrpc