# Processes  
本节旨在帮助我们的客户和我们的开源贡献者了解我们的工作方式以及我们使用的工具  
以下页面总结了我们最重要的软件开发流程——  **我们的工作方式** ——并链接到更详细的可用文档。  
## 01 Dokuwiki   多库维基   
介绍我们 Dokuwiki 的内容，以及有关如何创建或编辑内容的详细文档。  
[ 01 Dokuwiki](01%20Dokuwiki.md)
## 02 Version Control  版本控制  
关于如何设置 Git 以及如何使用它的详细说明。 还涵盖了我们的版本控制策略。  
[ 02 Version Control](02%20Version%20Control.md)  
## 03 Issue Tracking  问题追踪  
关于在哪里可以找到我们的问题跟踪以及如何贡献的信息。  
[ 03 Issue Tracking](03%20Issue%20Tracking.md)  
## 04 Release Management  发布管理  
关于我们如何为我们开发的项目和应用程序创建新版本的详细信息，以及在哪里可以找到它们。  
[04 Release Management](04%20Release%20Management.md)  
## 05 Collaboration  协作  
有关如何根据我们的工作方式协作处理我们的源代码（封闭和开源）的信息。  
[05 Collaboration](05%20Collaboration.md) 